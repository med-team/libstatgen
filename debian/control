Source: libstatgen
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Dylan Aïssi <daissi@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               doxygen,
               libssl-dev,
               zlib1g-dev,
               d-shlibs
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/libstatgen
Vcs-Git: https://salsa.debian.org/med-team/libstatgen.git
Homepage: https://genome.sph.umich.edu/wiki/C++_Library:_libStatGen
Rules-Requires-Root: no

Package: libstatgen1
Architecture: any-amd64
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: processing and analyzing next generation sequencing and genotyping data library
 libStatGen is a library for statistical genetic programs. It includes some:
 A. General Operation Classes including: File/Stream I/O, String processing
  and Parameter Parsing.
 B. Statistical Genetic Specific Classes including: Handling Common file
  formats (Accessors to get/set values, Indexed access to BAM files) and
  some utility classes, including: 1. Cigar: interpretation and mapping
  between query and reference. 2. Pileup: structured access to data by
  individual reference position.
 .
 This package provides the shared library.

Package: libstatgen-dev
Architecture: any-amd64
Multi-Arch: same
Section: libdevel
Depends: libstatgen1 (= ${binary:Version}),
         ${misc:Depends}
Suggests: libstatgen-doc
Description: development files for the libStatGen
 libStatGen is a library for statistical genetic programs. It includes some:
 A. General Operation Classes including: File/Stream I/O, String processing
  and Parameter Parsing.
 B. Statistical Genetic Specific Classes including: Handling Common file
  formats (Accessors to get/set values, Indexed access to BAM files) and
  some utility classes, including: 1. Cigar: interpretation and mapping
  between query and reference. 2. Pileup: structured access to data by
  individual reference position.
 .
 This package provides the development files for libstatgen.

Package: libstatgen-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: libstatgen-dev (>= ${source:Version})
Description: documentation files for the libStatGen
 libStatGen is a library for statistical genetic programs. It includes some:
 A. General Operation Classes including: File/Stream I/O, String processing
  and Parameter Parsing.
 B. Statistical Genetic Specific Classes including: Handling Common file
  formats (Accessors to get/set values, Indexed access to BAM files) and
  some utility classes, including: 1. Cigar: interpretation and mapping
  between query and reference. 2. Pileup: structured access to data by
  individual reference position.
 .
 This package provides the documentation files for libstatgen.
